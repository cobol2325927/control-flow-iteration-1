       IDENTIFICATION DIVISION. 
       PROGRAM-ID. LISTING6-4.
       AUTHOR. MUTITA.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  OUNTERS.
           05 HUNDREDS-COUNT PIC 99 VALUE ZEROS.
           05 TENS-COUNT PIC 99 VALUE ZEROES.
           05 UNTIS-COUNT PIC 99 VALUE ZEROES .
       01  ODOMETER.
           05 PRN-HUNDREDS  PIC 9.
           05 FILLER        PIC X VALUE "-".
           05 PRN-TENS      PIC 9.
           05 FILLER        PIC X VALUE "-".
           05 PRN-UNITS     PIC 9.

       PROCEDURE DIVISION.
       000-BEGIN.
           DISPLAY "Using an out-of-ling Perform"
           PERFORM 001-COUNT-MILEAGE THRU 001-EXIT 
              VARYING HUNDREDS-COUNT FROM 0 BY 1
                 UNTIL HUNDREDS-COUNT > 9
               AFTER TENS-COUNT FROM 0 BY 1 UNTIL TENS-COUNT > 9
               AFTER UNTIS-COUNT FROM 0 BY 1 UNTIL UNTIS-COUNT > 9
           GOBACK 
       .

       001-COUNT-MILEAGE.
           MOVE HUNDREDS-COUNT TO PRN-HUNDREDS 
           MOVE TENS-COUNT TO PRN-TENS
           MOVE UNTIS-COUNT TO PRN-UNITS
           DISPLAY "Out - " ODOMETER 
       .

       001-EXIT.
           EXIT 
       .
           